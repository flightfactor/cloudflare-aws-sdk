package main

import (
	"context"
	"fmt"
	"path/filepath"
	"unsafe"

	// "unsafe"

	"log"

	"os"
	// "path/filepath"

	// "unsafe"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/feature/s3/manager"

	// "github.com/aws/aws-sdk-go-v2/feature/s3/manager"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

var (
	accountId         string
	access_key_id     string
	access_key_secret string
)

func init() {
	accountId = os.Getenv("CLOUDFLARE_ACCOUNT_ID")
	access_key_id = os.Getenv("ACCESS_KEY_ID")
	access_key_secret = os.Getenv("ACCESS_KEY_SECRET")

}

func main() {

	// k := 0

	// var allfiles []string

	// filepath.WalkDir("/mnt/seconddisk/repos/gitlab.com/flightfactor/cloudflare-r2/Aircraft/FlightFactor & StepToSky/Boeing 757 Professional", func(path string, file fs.DirEntry, err error) error {
	// 	if err != nil {
	// 		return err
	// 	}
	// 	if !file.IsDir() {
	// 		fmt.Println(path[118:])

	// 		k++

	// 		allfiles = append(allfiles, path[118:])

	// 	}

	// 	return nil
	// })

	// log.Println(k)

	var bucketName = "boeing-757-professional"
	var accountId = accountId
	var accessKeyId = access_key_id
	var accessKeySecret = access_key_secret

	r2Resolver := aws.EndpointResolverWithOptionsFunc(func(service, region string, options ...interface{}) (aws.Endpoint, error) {
		return aws.Endpoint{
			URL: fmt.Sprintf("https://%s.r2.cloudflarestorage.com", accountId),
		}, nil
	})

	cfg, err := config.LoadDefaultConfig(context.TODO(),
		config.WithEndpointResolverWithOptions(r2Resolver),
		config.WithCredentialsProvider(credentials.NewStaticCredentialsProvider(accessKeyId, accessKeySecret, "")),
	)
	if err != nil {
		log.Fatal(err)
	}

	client := s3.NewFromConfig(cfg)

	downloader := manager.NewDownloader(client)

	params := &s3.ListObjectsV2Input{
		Bucket: aws.String(bucketName),
	}

	paginator := s3.NewListObjectsV2Paginator(client, params, func(o *s3.ListObjectsV2PaginatorOptions) {
		o.Limit = 1000
	})

	// pageNum := 0
	for paginator.HasMorePages() {
		output, err := paginator.NextPage(context.TODO())
		if err != nil {
			log.Printf("error: %v", err)
			return
		}
		for _, value := range output.Contents {
			// fmt.Println(*value.Key)

			objkey := value.Key

			var key = (*string)(unsafe.Pointer(objkey))

			keystr := "boeing-757-professional/" + string(*key)

			if err := os.MkdirAll(filepath.Dir(keystr), 0755); err != nil {
				panic(err)
			}

			newFile, err := os.Create(keystr)
			if err != nil {
				log.Println(err)
			}
			defer newFile.Close()

			_, err = downloader.Download(context.TODO(), newFile, &s3.GetObjectInput{
				Bucket: aws.String("boeing-757-professional"),
				Key:    aws.String(*value.Key),
			})

			if err != nil {
				log.Fatal(err)
			}

		}
		// pageNum++
	}

	// listObjectsOutput, err := client.ListObjectsV2(context.TODO(), &s3.ListObjectsV2Input{
	// 	Bucket: &bucketName,

	// })
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// downloader := manager.NewDownloader(client)

	// // paginator := s3.NewListPartsPaginator(client, params)

	// k := 0

	// for _, object := range listObjectsOutput.Contents {

	// // for _, key := range allfiles {

	// 	objkey := object.Key

	// 	var key = (*string)(unsafe.Pointer(objkey))

	// 	keystr := "boeing-757-professional/" + string(*key)

	// 	// keystr := "boeing-757-professional/" + key

	// 	k++

	// 	log.Println(keystr, k)

	// 	if err := os.MkdirAll(filepath.Dir(keystr), 0755); err != nil {
	// 		panic(err)
	// 	}

	// 	newFile, err := os.Create(keystr)
	// 	if err != nil {
	// 		log.Println(err)
	// 	}
	// 	defer newFile.Close()

	// 	_, err = downloader.Download(context.TODO(), newFile, &s3.GetObjectInput{
	// 		Bucket: aws.String("boeing-757-professional"),
	// 		Key:    aws.String(*object.Key),
	// 	})

	// 	if err != nil {
	// 		log.Fatal(err)
	// 	}

	// }

	// listBucketsOutput, err := client.ListBuckets(context.TODO(), &s3.ListBucketsInput{})
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// for _, object := range listBucketsOutput.Buckets {

	// 	obj, _ := json.MarshalIndent(object, "", "\t")
	// 	fmt.Println(string(obj))
	// }

	// {
	// 		"CreationDate": "2022-05-18T17:19:59.645Z",
	// 		"Name": "sdk-example"
	// }
}
